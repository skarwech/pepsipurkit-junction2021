import './App.css';
//import sensorJson from './assets/site_6.json';
import anime from 'animejs';
import { useState, useEffect } from 'react';
import { ReactComponent as Footprint } from './assets/footprint.svg'
import { ReactComponent as Car } from './assets/car.svg'
import data from './assets/matin_huikea_data.json';
import luminous from './assets/luminous.png';

function App() {


  //const listItems = sensorJson.map((element) => <div style={{ position: 'absolute', left: (0.96 * element.x + 30), top: (element.y - 80), height: 20, width: 20, userSelect: "none" }} key={element.deviceid}><div>{element.deviceid}</div></div>)
  const [loopMap, setLoopMap] = useState(new Map());



  useEffect(() => {
    let loopMap = new Map();
    for (var i = 0; i < Object.keys(data).length; i++) {
      let eventObject = data[i];
      eventObject.duration = data[i].timestamp * 1000;
      eventObject.x = data[i].x;
      eventObject.y = data[i].y;
      let objtype = 0
      switch (data[i].objecttype) {
        case '':
          objtype = 0
          break;
        case 'Speech':
          objtype = 1
          break;
        case 'Silence':
          objtype = 2
          break;
        case 'auto':
          objtype = 3
          break;
        case 'Unknown':
          objtype = 4
          break;
        default:
          break;
      }
      eventObject.type = objtype
      let potentialArray = loopMap.get(eventObject.groupid);
      if (potentialArray !== undefined) {
        potentialArray.push(eventObject);
      } else {
        potentialArray = [eventObject]
      }
      loopMap.set(data[i].groupid, potentialArray);
    }
    console.log("loopmap")
    console.log(loopMap)
    setLoopMap(loopMap);
    for (const [key, value] of loopMap.entries()) {

      const arrayOfEvents = value;

      var previousX;
      var previousY;
      var lastTimestamp;
      var startingPoint = Math.round(arrayOfEvents[0].timestamp)

      var xAnimationArray = [];
      var yAnimationArray = [];
      var endingTime = arrayOfEvents[arrayOfEvents.length - 1].timestamp;
      for (let arrayI = 0; arrayI < arrayOfEvents.length; arrayI++) {
        var time = 0;
        var duration = 1000;
        if (arrayI !== 0) {
          let diffX = -((previousX * 0.96) - (arrayOfEvents[arrayI].x * 0.96));
          let diffY = -((previousY) - (arrayOfEvents[arrayI].y));
          var degree = 0;


          if (key === 5) {

            console.log("//DEGREE//")
            console.log(diffX)
            console.log(diffY)
            console.log(diffX / diffY)
            console.log(degree)
          }

          if (lastTimestamp === 0) {
            time = 0;
            lastTimestamp = 100;
          } else if (arrayI < arrayOfEvents.length - 1) {
            time = Math.round(arrayOfEvents[arrayI].timestamp);
            duration = (arrayOfEvents[arrayI].timestamp - arrayOfEvents[arrayI].timestamp + 1) * 1000
          } else {
            duration = 100;
            time = endingTime;
          }

          let xKeyFrame = {
            value: '+=' + diffX + 'px',
            delay: startingPoint * 50 + time * 10,
            duration: duration * 10,
          }

          let yKeyFrame = {
            value: '+=' + diffY + 'px',
            delay: startingPoint * 50 + time * 10,
            duration: duration * 10,
          }
          xAnimationArray.push(xKeyFrame);
          yAnimationArray.push(yKeyFrame);
        }
        previousX = arrayOfEvents[arrayI].x;
        previousY = arrayOfEvents[arrayI].y
      }

      var opacityStart = {
        value: 0,
        time: 0,
        delay: 0,
      }
      var opacityEnd = {
        value: 1,
        time: 100,
        delay: startingPoint * 50
      }
      var opacityFade = {
        value: 0,
        time: 100,
        delay: startingPoint * 50 + endingTime * 100
      }
      if (yAnimationArray.length !== 0 && xAnimationArray.length !== 0) {
        const keyFile = '#element' + key
        anime({
          targets: keyFile,
          easing: 'easeInOutQuad',
          translateX: xAnimationArray,
          translateY: yAnimationArray,
          opacity: [opacityStart, opacityEnd, opacityFade],
          autoplay: true,
        })
      }
    }
  }, [])

  function domAnimationItems() {
    if (loopMap !== undefined) {
      var returnArray = [];
      var alreadyUsedIds = [];
      for (var i = 0; i < Object.keys(data).length; i++) {
        if (!alreadyUsedIds.includes(data[i].groupid)) {
          alreadyUsedIds.push(data[i].groupid)
          returnArray.push(
            <div id={'element' + data[i].groupid} style={{ position: 'absolute', left: (data[i].x * 0.96 + 30), top: (data[i].y - 80), opacity: 0 }}>
              {(data[i].type === 0 || data[i].type === 1) && (
                <Footprint id={'icon' + data[i].groupid} style={{ height: 52, width: 52, userSelect: "none", position: 'absolute' }} key={data[i].groupid} ></Footprint>
              )}
              {(data[i].type === 3 || data[i].type === 4) && (
                <Car style={{ height: 52, width: 52, userSelect: "none", position: 'absolute' }} key={data[i].groupid} >{data[i].x}</Car>
              )}
              <img src={luminous} alt="fireSpot" style={{ opacity: 0.2, marginLeft: -50, marginTop: -50 }} />
            </div>)
        }
      }
      return returnArray
    } else {
      return "TYHJÄ"
    }
  }
  return (
    <div className='w-full h-full overflow-auto flex flex-col background-color bg-background-template'>
      <div className="w-screen h-screen overflow-auto">
        <div className="w-full h-full overflow-auto bg-floor-plan bg-no-repeat m-auto">
          <div className="w-full h-full">
            <div>
            </div>
            <div className="w-full h-full">   {domAnimationItems()}
            </div>
          </div>
        </div>
      </div>
    </div >
  );
}

export default App;
