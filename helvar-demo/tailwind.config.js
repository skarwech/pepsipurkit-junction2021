module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'], darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: {
        'floor-plan': "url('./assets/site_6_new.png')",
        'background-template': "url('./assets/tausta.png')",
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
