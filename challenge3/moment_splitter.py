from matplotlib import pyplot as plt
import librosa
import librosa.display
import numpy as np
import scipy as sp
from scipy.fft import fft
from scipy.io.wavfile import read
from scipy.io.wavfile import write
from scipy import signal
import random
import string


def moment_splitter(offset=0, sensor=1, path="./audio_data/", duration=1, highpass=10000, lowpass=10, bandstop1=[0, 0], bandstop2=[0, 0]):
    test_aud_file = path + "audio_sensor_" + str(sensor) + "_filtered.wav"
    aud, SR = librosa.load(test_aud_file, sr=22050,
                           offset=offset, duration=duration)

    array = aud
    Frequency = SR
    """
    FourierTransformation = fft(array)
    scale = sp.linspace(0, Frequency, len(array))
    FourierTransformation = fft(array)

    scale = sp.linspace(0, Frequency, len(array))

    GuassianNoise = np.random.randn(len(FourierTransformation))

    b, a = signal.butter(5, highpass/(Frequency/2), btype='highpass')
    filteredSignal = signal.lfilter(b, a, array)

    c, d = signal.butter(5, lowpass/(Frequency/2), btype='lowpass')
    newFilteredSignal = signal.lfilter(c, d, filteredSignal)

   # c,d = signal.butter(5, [bandstop1[0]/(Frequency/2), bandstop1[1]/(Frequency/2)], btype='bandstop')
   # newFilteredSignal = signal.lfilter(c,d,filteredSignal)

   # c,d = signal.butter(5, [bandstop2[0]/(Frequency/2), bandstop2[1]/(Frequency/2)], btype='bandstop')
   # newFilteredSignal = signal.lfilter(c,d,filteredSignal)

    rand_str = ''.join(random.choices(string.ascii_lowercase, k=6))
    filename = "filtered_sensor_"+str(sensor)+"_" + rand_str + ".wav"
    """
    #write(filename, Frequency, np.int16(newFilteredSignal/np.max(np.abs(newFilteredSignal)) * 32767))

    #aud, SR = librosa.load(filename, sr=22050)
    filename = ""
    return aud, SR, filename
